const path = require('path');

module.exports = function axessibility (_options) {
  const defaultOptions = {}
  
  let options = Object.assign(defaultOptions, _options);
  if(this.options.globalName) options['globalName'] = this.options.globalName;
  
  this.addPlugin({
    src: path.resolve(__dirname, 'plugin.js'),
    fileName: 'nuxt-axessibility.js',
    options
  })
}

module.exports.meta = require('../package.json')