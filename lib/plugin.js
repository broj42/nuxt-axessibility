let options = <%= serialize(options) %>;

const capitalize = (s) =>{
  if(typeof s !== 'string') return ''
  return s.charAt(0).toUpperCase() + s.slice(1)
}

if(process.client){
  let globalName = capitalize(options.globalName) || 'Nuxt';
  window[`on${globalName}Ready`](async () => {
    let Axessibility = await import('axessibility')
    Axessibility = new Axessibility.default(options)
  })
}