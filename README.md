# nuxt-axessibility

[![npm version][npm-version-src]][npm-version-href]
[![npm downloads][npm-downloads-src]][npm-downloads-href]
[![npm downloads][kofi-src]][kofi-href]

####
Wrapper for **[axessibility.js](https://www.npmjs.com/package/axessibility/v/latest)**
#####
Try it out here
**[nuxt-axessibility](https://codesandbox.io/s/nuxt-axessibility-j37qu)**

## 🚀 Usage
```bash
npm i nuxt-axessibility
```

```javascript
// nuxt.config.js
modules: [
  ['nuxt-axessibility', {
    // your options
  }]
]
```
## 🔧 Options
| option | type | default |
| ------ | ------ | ------ |
| position | String | 'bottom-left' |
| background | String (HEX/RGB/RGBA) | '#253786' |
| color | String (HEX/RGB/RGBA) | '#fff' |
| contrast | Boolean | true |
| images | Boolean | true |
| largeCursor | Boolean | true |
| readingLine | Boolean | true |
| fontSize | Boolean | true |
| fontSizeStep | Integer | 2 |
| fontSizeMaxSteps | Integer | 4 |
| filters | Array (css filters) | ['grayscale', 'invert', 'sepia'] |

### Buy me a coffee
[![ko-fi](https://www.ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/F1F31MWWL)


<!-- Badges -->
[npm-version-src]: https://badgen.net/npm/v/nuxt-axessibility/latest
[npm-version-href]: https://npmjs.com/package/nuxt-axessibility

[kofi-src]: https://badgen.net/badge/icon/kofi?icon=kofi&label=support
[kofi-href]: https://ko-fi.com/darioferderber

[npm-downloads-src]: https://badgen.net/npm/dm/nuxt-axessibility
[npm-downloads-href]: https://npmjs.com/package/nuxt-axessibility